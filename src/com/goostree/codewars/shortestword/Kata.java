package com.goostree.codewars.shortestword;

import java.util.Arrays;

public class Kata {
	public static int findShort(String s) {        
        return Arrays.stream(s.split(" "))
        		   	 .mapToInt(i -> i.length())
        			 .min().getAsInt();
        // Arrays.stream(s.split(" "))
		//		 		  .min((o1, o2) -> Integer.compare(o1.length(), o2.length()))
        //                .get().length()
    }
}
